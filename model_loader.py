import os

import matplotlib.pyplot as plt
import numpy as np
import tensorflow as tf

# fixes cpu bug
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'
# corresponding label names for classification
class_names = ['T-shirt/top', 'Trouser', 'Pullover', 'Dress', 'Coat', 'Sandal', 'Shirt', 'Sneaker', 'Bag', 'Ankle boot']

# Hier Optimierungsalgorithmus auswählen
# my_model_nadam
# my_model_rmsprop
model_name = 'my_model_nadam'

# filepaths for train and test data
test_file_path = "./fashionmnist/fashion-mnist_test.csv"

# loading data from csv
test_data = np.loadtxt(test_file_path, delimiter=",")

# normalize and separating label and image data
# factor for normalization
fac = 1 / 255

# separating test images and applying the normalfactor
test_imgs = np.asfarray(test_data[:, 1:]) * fac

# separating first indices of every array as they are the test labels
test_labels = np.asfarray(test_data[:, :1])

new_model = tf.keras.models.load_model(model_name)
test_loss, test_acc = new_model.evaluate(test_imgs, test_labels, verbose=2)
print('\nTest accuracy:', test_acc)

# make predictions
probability_model = tf.keras.Sequential([new_model, tf.keras.layers.Softmax()])
predictions = probability_model.predict(test_imgs)
rnd = np.random.randint(0, 9999)

# prediction output
print("predicted value: ")
print(np.argmax(predictions[rnd]))
print(class_names[np.argmax(predictions[rnd])])

# certainty
print("confidence in percent: ")
print(predictions[rnd][np.argmax(predictions[rnd])] * 100)

# actual value
print("expected value: ")
print(test_labels[rnd][0].astype(int))
print(class_names[test_labels[rnd][0].astype(int)])

# plotting image
img = test_imgs[rnd].reshape((28, 28))
plt.imshow(img, cmap="Greys")
plt.show()
