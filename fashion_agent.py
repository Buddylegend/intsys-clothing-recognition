import datetime
import os

import numpy as np
import tensorflow as tf
from tensorflow import keras

# fixes cpu bug
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'

# corresponding label names for classification
class_names = ['T-shirt/top', 'Trouser', 'Pullover', 'Dress', 'Coat', 'Sandal', 'Shirt', 'Sneaker', 'Bag', 'Ankle boot']

# filepaths for train and test data
train_file_path = "./fashionmnist/fashion-mnist_train.csv"

# loading data from csv
train_data = np.loadtxt(train_file_path, delimiter=",")

# normalize and separating label and image data
# factor for normalization
fac = 1 / 255

# separating train images and applying the normalfactor
train_imgs = np.asfarray(train_data[:, 1:]) * fac
# separating first indices of every array as they are the train labels
train_labels = np.asfarray(train_data[:, :1])

# set up layers
# ToDo optimize
model = keras.Sequential([
    keras.layers.Flatten(),
    keras.layers.Dense(128, activation='relu'),
    keras.layers.Dense(10, activation='softmax')
])

# compile the model
model.compile(optimizer='nadam',
              loss=tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True),
              metrics=['accuracy'])

# log tensorboard
log_dir = "logs/fit/" + datetime.datetime.now().strftime("%Y%m%d-%H%M%S")
tensorboard_callback = tf.keras.callbacks.TensorBoard(log_dir=log_dir, histogram_freq=1)

# feed the model with train data
model.fit(train_imgs, train_labels, epochs=200)

# saving the trained model
model.save('my_model_nadam')
